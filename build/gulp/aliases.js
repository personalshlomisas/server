/**
 * Created by Shlomi on 07/08/2015.
 */

import gulp from 'gulp';
import runSequence from 'run-sequence';

runSequence.use(gulp);

gulp
    .task('build', function(callback){
        runSequence('prv-clean-dist', 'prv-config', 'prv-babel', callback);
    })
    .task('default', ['build']);
