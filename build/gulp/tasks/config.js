/**
 * Created by Shlomi on 28/06/2015.
 */

export default (gulp, plugins, options) => {
    return () => {

        var props = {
            env: options.env || 'dev'
        };

        return gulp.src(['./assets/config/base.json', './assets/config/' + props.env +'.json'])
            .pipe(plugins.extend('./config.json', true, '\t'))
            .pipe(gulp.dest('./'));
    };
};