# Palo Alto Networks Exam - Server

## Clone the project

run `git clone git@bitbucket.org:personalshlomisas/server.git && cd server`

## Install

run `npm install`

## Build

run `npm run build`

## Start

run `npm start`