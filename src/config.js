/**
 * Created by Shlomi on 29/06/2015.
 *
 */

import nconf from 'nconf';
import path from 'path';

import packageJson from '../package.json';

const configJSON = path.resolve(__dirname, '..', 'config.json');

nconf
    .argv()
    .env()
    .file('global', { file: configJSON })
    .set('version', packageJson.version);

export default nconf;