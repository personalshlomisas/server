/**
 * Created by Shlomi on 29/05/2016.
 */

import {EventEmitter} from 'events';
import helper from './helper';
import config from '../config';
import Move from './move';

export default class Game extends EventEmitter{

    static _startGameAt = config.get('game:startAt');

    _lock = false;
    _players = [];
    _scores = {};

    _currPlayerIndex = 0;

    /**
     *
     * @param player Player
     */
    join(player){
        if(this._lock) throw new Error('Game in action..');

        this._players.push(player);
        this._scores[player.getName()] = 0;

        helper.log(`Player ${player} has been added to the game`);
    }

    needToStart(){
        return this._players.length >= Game._startGameAt;
    }

    isGameRunning(){
        return this._lock;
    }

    getCurrentPlayer() {

        if(!this.isGameRunning()) throw new Error('Game is not running');

        return this._players[this._currPlayerIndex];
    }

    scoreMove(move){
        // @todo: what is the scoring logic??
        return move.getNumber()*2;
    }

    start(){
        this._lock = true;
        this.emit('game:started');

        this.makeMove();

    }

    makeMove() {
        if(!this.isGameRunning()) throw new Error('Game is not running');
        if(this._currPlayerIndex > (this._players.length - 1)) return this.end();

        this.emit('move:make', this.getCurrentPlayer());
    }

    moveMade(player, move) {

        if(!this.isGameRunning()) throw new Error('Game is not running');

        let currPlayer = this.getCurrentPlayer();

        if(currPlayer.getName() !== player.getName()) throw new Error(`Player ${player} trying to play but it's not his turn`);

        if(move.getAction() === Move.HOLD_ACTION){
            this._currPlayerIndex++;
        }else{
            this._scores[player.getName()] += this.scoreMove(move);
        }

        this.emit('move:made', player, move);

        this.makeMove();
    }

    end(){

        if(!this.isGameRunning()) throw new Error('Game is not running');

        this.emit('game:ended', this._scores);

        this._lock = false;
        this._currPlayerIndex = 0;
        this._players = [];
        this._scores = {};

    }


}
