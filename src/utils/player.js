/**
 * Created by Shlomi on 29/05/2016.
 */

import {EventEmitter} from 'events';

import helper from './helper';

export default class Player extends EventEmitter{

    _name;

    constructor(data){
        super();
        this._name = data.name;
    }

    toJSON(){
        return {
            name: this._name
        }
    }

    toString(){
        return `Player ${this._name}`;
    }

    getName(){
        return this._name;
    }
}
