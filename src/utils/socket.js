
export default class Socket{

    _socket;

    constructor(socket){
        this._socket = socket;
    }

    on(){
        this._socket.on(...arguments);
    }

    emit(){
        this._socket.emit(...arguments);
    }
}