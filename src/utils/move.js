/**
 * Created by Shlomi on 29/05/2016.
 */

import helper from './helper';

export default class Move{

    static ROLL_ACTION = 'roll';
    static HOLD_ACTION = 'hold';

    _action;
    _number;

    constructor(data = {}){
        
        this._action = data.action;
        
        if(this._action === Move.ROLL_ACTION){
            this._number = data.number;
        }
    }

    toJSON(){
        return {
            action: this._action,
            number: this._number
        }
    }

    toString(){
        let str = `Move - ${this._action}`;
        if(this._action === Move.HOLD_ACTION) return str;
        return `${str}, number: ${this._number}`;
    }

    getAction(){
        return this._action;
    }

    getNumber(){
        return this._number;
    }

}
