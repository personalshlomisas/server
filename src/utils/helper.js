export default {
    log(){
        console.log(...arguments);
    },

    debug(){
        this.log(...arguments);
    },

    error(){
        console.trace(...arguments);
    },

    printScores(scores){
        this.debug(`Final scores:`);
        Object.keys(scores).forEach(key => {
            this.debug(`Player ${key}: ${scores[key]}`);
        });
    },

    exit(e){
        if(e) this.error(e);
        process.exit();
    }
}