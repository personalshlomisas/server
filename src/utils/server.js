
import {EventEmitter} from 'events';
import sio from 'socket.io';

import helper from './helper';
import config from '../config';

export default class Server extends EventEmitter{

    _io;

    constructor(){
        super();
        this._io = sio();

        this._io.on('connection', socket => {
            this.emit('connection', socket);
        });
    }

    start(){

        let port = config.get('socketio:port');

        helper.debug(`Starting server at port ${port}`);
        this._io.listen(port);
        return Promise.resolve();
    }

    broadcast(){
        this._io.emit(...arguments);
    }
}