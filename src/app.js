/**
 * Created by Shlomi on 29/05/2016.
 */

import helper from './utils/helper';

import Server from './utils/server';
import Socket from './utils/socket';
import Player from './utils/player';
import Move from './utils/move';
import Game from './utils/game';

// ###### Create server instance

const server = new Server();

// Listen to server new connections
server.on('connection', sioSocket => {

    helper.debug(`Got new connection`);

    let socket = new Socket(sioSocket);

    // ###### Create player instance

    // Socket events
    socket.on('game:join', data => {

        let player = new Player(data.player);

        helper.debug(`Player ${player} has asked to join the game..`);

        socket.on('move:made', data => {
            if(!data || !data.move) return;

            try{
                game.moveMade(player, new Move(data.move));
            }catch(e){
                helper.error(e);
                socket.emit('error', e.message);
            }
        });

        try{
            game.join(player);

            if(game.needToStart()){
                helper.debug(`Starting the game..`);
                game.start();
            }
        }catch(e){
            helper.error(e);
            socket.emit('myError', e.message);
        }
    });

    socket.on('error', e => {
        helper.exit(e);
        socket.emit('error', e);
    });

    // Report client server is ready for him connection
    socket.emit('server:ready');

});

// ###### Create game instance

const game = new Game();

// When game turn started, broadcast who's going to play
game.on('turn:start', player => {

    helper.debug(`Game round started, player is ${player}`);

    server.broadcast('move:start', {
        player
    });
});

// When game turn's move REQUIRE to make, broadcast who's supposed to do that
game.on('move:make', (player, move) => {

    helper.debug(`Game move need to make by ${player}`);

    server.broadcast('move:make', {
        player
    });
});

// When game turn's move made, broadcast who's playing and the move he made
game.on('move:made', (player, move, scores) => {

    helper.debug(`Game move [${move}] has been made by ${player}`);

    server.broadcast('move:made', {
        player,
        move,
        scores
    });
});

// When game turn ended, broadcast the player and the round's results
game.on('turn:ended', (player, results) => {

    helper.debug(`Game round ended for ${player}, results: ${results}`);

    server.broadcast('move:start', {
        player,
        results
    });
});

// When game is ended, broadcast that and exist the process
game.on('game:ended', scores => {

    const time = 2000;

    helper.debug(`Game has ended`);
    helper.printScores(scores);
    helper.debug(`Exiting in ${time} ms..`);

    server.broadcast('game:ended', scores);

    setTimeout(() => {
        // Exit process
        helper.exit();
    }, time);
});

// Start the server..
server.start();